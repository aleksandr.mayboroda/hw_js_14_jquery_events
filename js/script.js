$('.top-menu-link').on('click',function() {
    let blockId = $(this).attr('href')
    $("html, body").animate({
        scrollTop: $(blockId).offset().top + 3 + "px" // .top+margin - ставьте минус, если хотите увеличить отступ
     }, {
        duration: 1600, // тут можно контролировать скорость
        // easing: "swing"
     });
    console.dir($(blockId).offset())
})


$(document).scroll(toggleScrollBtn)
toggleScrollBtn()
function toggleScrollBtn() {
    let toTopBtn = $('.to-top-container')
    
    if($(this).scrollTop() >= $(window).height()) //прокручено - высота экрана
    {
        toTopBtn.fadeIn(1000)
        console.log('ooooo')
    }
    else
    {
        toTopBtn.fadeOut(1000)
        console.log($(this).scrollTop(), $(window).height())
    }
  
}

// click on btn
$("body").on('click',toTopClick)
function toTopClick(evnt) {

    //scroll to top
    if(evnt.target.closest('.to-top-container'))
    {
        $('html, body').animate(
        {
            scrollTop: '0px'   
        },
        {
            duration: 500,
            easing: "swing"
        })
    }

    //toggle posts block
    if(evnt.target.dataset.action)
    {
        let postsBlock = $('.' + evnt.target.dataset.action)
        postsBlock.toggle()
        if(postsBlock.css('display') === 'none')
        {
            evnt.target.innerText = 'Show'
        }
        else
        {
            evnt.target.innerText = 'Hide'
        }
       
        // console.log(postsBlock.css('display'))
    }
}